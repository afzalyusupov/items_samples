from import_export import resources
from mptt import utils

from . import models


class ItemResource(resources.ModelResource):
    class Meta:
        model = models.Item
        use_transactions = True
        fields = (
            'name',
            'rank',
            'quantity',
            'verified',
            'category',
            'updated_at',
            'created_at',
        )

    #
    # def loop_over_children(self, category):
    #     if category.children.count():
    #         self.loop_over_children(category)
    #     else:
    #         return category.name
    #
    # def dehydrate_category(self, parent):
    #     children = list(parent.category.all())
    #
    #     out = ""
    #     for i, child in enumerate(children):
    #         if i == 0:
    #             self.loop_over_children(child)
    #             out += f'{ child.name }'
    #         else:
    #             self.loop_over_children(child)
    #             out += f';{ child.name }'
    #
    #     return out

    # def build_category_tree(self, category, previous=None):
    #     if category.children.count():
    #
    #
    # def dehydrate_category(self, parent):
    #     cat_tree = self.build_category_tree(category=parent)

    def dehydrate_category(self, parent):
        utils.tree_item_iterator()
        pass

    # def dehydrate_category(self, parent):
    #     children = list(parent.category.all())
    #
    #     out = ""
    #     for i, child in enumerate(children):
    #         if i == 0:
    #             out += f'{ child.name }'
    #         else:
    #             out += f';{ child.name }'
    #
    #     return out

    def dehydrate_verified(self, parent):
        return True if parent.verified else False
