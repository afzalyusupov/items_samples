from django.contrib import admin
import csv
from django.http import HttpResponse
from mptt.admin import DraggableMPTTAdmin
from django.contrib.auth.models import Group
from import_export.admin import ImportExportModelAdmin

from . import models, resources

admin.site.unregister(Group)


class ExportCsvMixin:
    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = "Export Selected"


@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    fields = (
        'first_name', 'last_name', 'email', 'department', 'grant_access_to_this_account', 'notify_date', 'expiration_date'
    )
    list_display = ('first_name', 'email', 'grant_access_to_this_account')


@admin.register(models.Favourite)
class FavouriteAdmin(admin.ModelAdmin):
    pass

#
# @admin.register(models.Sample)
# class SampleAdmin(admin.ModelAdmin, ExportCsvMixin):
#     fields = ('id', 'item', 'date', 'name', 'text')
#     list_display = ('id', 'name', 'text')
#     actions = ["export_as_csv"]


admin.site.register(models.Category, DraggableMPTTAdmin)


@admin.register(models.Item)
class usrdet(ImportExportModelAdmin):
    fields = ('name', 'rank', 'quantity', 'verified')
    list_display = ('name', 'rank', 'quantity')
    resource_class = resources.ItemResource


@admin.register(models.Sample)
class SampleAdmin(ImportExportModelAdmin):
    list_display = ('id', 'name', 'text')
